export interface ReportStatus  {
    id: number;
    value: string;
}