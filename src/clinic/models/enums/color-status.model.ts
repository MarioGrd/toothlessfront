export interface ColorStatus {
    name: string;
    status: string;
    value: number;
    primary: string;
    secondary: string;
}