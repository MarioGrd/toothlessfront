export interface ProcedureType {
    id: number;
    name: string;
  }