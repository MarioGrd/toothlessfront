export interface AppointmentStatus  {
    id: number;
    value: string;
}