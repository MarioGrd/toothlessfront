export interface GetReportQuery {
    ClinicId: string;
    ProcedureId?: string;

    StartTime: string;
    EndTime: string;
}