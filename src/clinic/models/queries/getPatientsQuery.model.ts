import {Pagination } from '../pagination.model';

export interface GetPatientsQuery extends  Pagination {
    clinicId: string;
}