export interface SearchClinicProcedureQuery {
    name: string;
    clinicId: string;
}