export interface SearchClinicPatientQuery {
    clinicId: string,
    lastName: string
}