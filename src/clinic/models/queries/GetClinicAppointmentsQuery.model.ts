export interface GetClinicAppointmentsQuery {
    ClinicId: string,
    StartTime: string,
    EndTime: string
}