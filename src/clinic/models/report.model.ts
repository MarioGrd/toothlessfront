import { ReportItem } from './report.item.model';
import { ReportData } from './report.data.model';

export interface Report {

    data: ReportData;
    items: ReportItem[];
}