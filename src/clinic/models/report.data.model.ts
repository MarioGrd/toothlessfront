export interface ReportData {
    missedCount: number;
    missedTotalTime: number;
    completedCount: number;
    completedTotalTime: number;
    unusedCount:number;
    unusedTotalTime: number;
}