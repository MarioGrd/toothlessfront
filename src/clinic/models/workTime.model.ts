export interface WorkTime {
    startHour: number,
    startMinute: number,
    endHour: number,
    endMinute: number
}