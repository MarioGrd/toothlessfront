import { ColorStatus } from '../enums/color-status.model'
import { AppointmentStatus } from '../enums/appointment-status.model'
import { ReportStatus } from '../enums/report-status.model';

export const COLOR_STATUS: ColorStatus[] = [
    {
      name: "yellow",
      status: "CREATED",
      value: 1,
      primary: "#e3bc08",
      secondary: "#FDF1BA"
    },
    {
      name: "blue",
      status: "COMPLETED",
      value: 2,
      primary: "#1e90ff",
      secondary: "#D1E8FF"
    },
    {
      name: "red",
      status: "DELETED",
      value: 3,
      primary: "#ad2121",
      secondary: "#FAE3E3"
    }
  ];
  
  export const APPOINTMENT_STATUS: AppointmentStatus[] = [
    { id: 1, value: "CREATED" },
    { id: 2, value: "COMPLETED" },
    { id: 3, value: "MISSED" }
  ];

  export const REPORT_STATUS: ReportStatus[] = [
    { id: 1, value: "UNUSED" },
    { id: 2, value: "COMPLETED" },
    { id: 3, value: "MISSED" }
  ];