export interface Paged<T> {
    data: T[],
    pageNumber: number,
    pageSize: number,
    count: number
}