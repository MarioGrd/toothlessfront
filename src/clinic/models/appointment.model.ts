export interface Appointment {
    id: string,
    startDate: Date,
    endDate: Date,
    appointmentStatusId: number,
    patient: string
}