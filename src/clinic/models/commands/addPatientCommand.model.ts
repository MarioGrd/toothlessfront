export interface AddPatientCommand {
    ClinicId: string;
    FirstName: string;
    LastName: string,
    Address: string,
    TelephoneNumber: string;
    BirthDate: Date;
}