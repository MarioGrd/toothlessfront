export interface AddClinicProcedureCommand {
    ClinicId: string,
    Name: string,
    Price: number,
    TypeId: number
}