export interface AddAppointmentCommand {

    clinicId: string;
    patientId: string;
    procedureId: string;
    startTime: string;
}