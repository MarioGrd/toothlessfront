export interface MarkAppointmentCompletedCommand {
    appointmentId: string;
}