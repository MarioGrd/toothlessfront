export interface MarkAppointmentMissedCommand {
    appointmentId: string;
}