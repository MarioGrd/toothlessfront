export interface Patient {
    id: string,
    name: string,
    telephoneNumber: string
}