import { WorkTime } from './workTime.model';
import { Procedure } from './procedure.model';

export interface Clinic {
    id: string,
    name: string,
    doctor: string,
    workTime: WorkTime
    procedures: Procedure[]
}