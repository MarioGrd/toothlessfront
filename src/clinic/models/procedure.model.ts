export interface Procedure {
    id: string,
    name: string,
    price: number,
    minutes: number,
    type: number
}