export interface Pagination {
    PageNumber: number,
    PageSize: number
}