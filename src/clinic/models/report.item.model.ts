export interface ReportItem {

    id?: string;
    startDate: Date;
    endDate: Date;
    price: number;
    appointmentStatusId?: number;
}