import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import {
    startOfWeek,
    endOfWeek
  } from "date-fns";



import { Store } from '@ngrx/store';

import { Observable, of } from 'rxjs';
import { tap, filter, take, switchMap, catchError } from 'rxjs/operators';

import * as fromStore from '../store';
import { GetReportQuery} from '../models/queries/GetReportQuery.model'

@Injectable()
export class ReportGuard implements CanActivate {
    
    clinicId: string;

    constructor(private store: Store<fromStore.OrdinationState>) {
    }

    canActivate(): Observable<boolean> {

        this.store.select(fromStore.getClinicId).subscribe(id => this.clinicId = id);

        var today = new Date();
        let query: GetReportQuery = {
            ClinicId: this.clinicId,
            StartTime: startOfWeek(today).toUTCString(),
            EndTime: endOfWeek(today).toUTCString()
          };
        this.store.dispatch(new fromStore.LoadReports(query));

        return this.store.select(fromStore.getReportLoaded)
        .pipe(
            filter(loaded => loaded),
            take(1)
        )
    }

}