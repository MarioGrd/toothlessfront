import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable, of } from 'rxjs';
import { tap, filter, take, switchMap, catchError } from 'rxjs/operators';


import * as fromStore from '../store';

@Injectable()
export class ClinicGuard implements CanActivate {
    
    clinicId: string;

    constructor(private store: Store<fromStore.OrdinationState>) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        this.clinicId = route.paramMap.get('id');
        return this.checkStore()
            .pipe(
                switchMap(() => of(true)),
                catchError(() => of(false))
            )
    }

    checkStore(): Observable<boolean> {
        return this.store.select(fromStore.getClinicLoaded)
            .pipe(
                tap(loaded => {
                    if(!loaded) {
                        this.store.dispatch(new fromStore.LoadClinic(this.clinicId));
                    }
                }),
                filter(loaded => loaded),
                take(1)
            );
    }
}