import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import {
    startOfMonth,
    endOfMonth
  } from "date-fns";

import { Store } from '@ngrx/store';

import { Observable, of } from 'rxjs';
import { tap, filter, take, switchMap, catchError } from 'rxjs/operators';

import * as fromStore from '../store';
import { GetClinicAppointmentsQuery} from '../models/queries/GetClinicAppointmentsQuery.model'
import { environment } from '../../environments/environment';

@Injectable()
export class AppointmentGuard implements CanActivate {
    
    clinicId: string;
    constructor(private store: Store<fromStore.OrdinationState>) {
    }

    canActivate(): Observable<boolean> {

        this.store.select(fromStore.getClinicId).subscribe(id => this.clinicId = id)

        return this.checkStore()
            .pipe(
                switchMap(() => of(true)),
                catchError(() => of(false))
            )
    }

    checkStore(): Observable<boolean> {
        return this.store.select(fromStore.getAppoinmentsLoaded)
            .pipe(
                tap(loaded => {
                    if(!loaded) {

                        var today = new Date();
                        let query: GetClinicAppointmentsQuery = {
                            ClinicId: this.clinicId,
                            StartTime: startOfMonth(today).toUTCString(),
                            EndTime: endOfMonth(today).toUTCString()
                          };
                        this.store.dispatch(new fromStore.LoadAppointments(query));
                    }
                }),
                filter(loaded => loaded),
                take(1)
            );
    }
}