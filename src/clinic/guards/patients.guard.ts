import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { Store } from '@ngrx/store';

import { Observable, of } from 'rxjs';
import { tap, filter, take, switchMap, catchError } from 'rxjs/operators';

import * as fromStore from '../store';
import { GetPatientsQuery} from '../models/queries/getPatientsQuery.model';

@Injectable()
export class PatientsGuard implements CanActivate {
    
    clinicId: string;
    constructor(private store: Store<fromStore.OrdinationState>) {}

    canActivate(): Observable<boolean> {
        this.store.select(fromStore.getClinicId).subscribe(id => this.clinicId = id)
        
        return this.checkStore()
            .pipe(
                switchMap(() => of(true)),
                catchError(() => of(false))
            )
    }

    checkStore(): Observable<boolean> {
        return this.store.select(fromStore.getPatientsLoaded)
            .pipe(
                tap(loaded => {
                    if(!loaded) {

                        let query: GetPatientsQuery = {
                            clinicId: this.clinicId,
                            PageNumber: 1,
                            PageSize: 10
                          };
                        this.store.dispatch(new fromStore.LoadPatients(query));
                    }
                }),
                filter(loaded => loaded),
                take(1)
            );
    }
}