import { ClinicGuard } from './clinic.guard';
import { PatientsGuard } from './patients.guard';
import { AppointmentGuard } from './appointment.guard';
import { ReportGuard } from './report.guard';

export const guards: any[] = [ClinicGuard, PatientsGuard, AppointmentGuard, ReportGuard];

export * from './clinic.guard';
export * from './patients.guard';
export * from './appointment.guard';
export * from './report.guard';