import { Action } from '@ngrx/store';
import { Appointment } from '../../models/appointment.model';
import { GetClinicAppointmentsQuery } from '../../models/queries/GetClinicAppointmentsQuery.model';

import { MarkAppointmentMissedCommand } from '../../models/commands/MarkAppointmentMissedCommand.model';
import { MarkAppointmentCompletedCommand } from '../../models/commands/MarkAppointmentCompletedCommand.model';
import { AddAppointmentCommand } from '../../models/commands/AddAppointmentCommand.model';

export const LOAD_APPOINTMENTS = '[Appointments] Load Appointments';
export const LOAD_APPOINTMENTS_FAIL = '[Appointments] Load Appointments Fail';
export const LOAD_APPOINTMENTS_SUCCESS = '[Appointments] Load Appointments Success';

export const MARK_APPOINTMENT_COMPLETED = '[Appointments] Mark Appointment Completed';
export const MARK_APPOINTMENT_COMPLETED_FAIL = '[Appointments] Mark Appointment Completed Fail';
export const MARK_APPOINTMENT_COMPLETED_SUCCESS = '[Appointments] Mark Appointment Completed Success';

export const MARK_APPOINTMENT_MISSED = '[Appointments] Mark Appointment Missed';
export const MARK_APPOINTMENT_MISSED_FAIL = '[Appointments] Mark Appointment Missed Fail';
export const MARK_APPOINTMENT_MISSED_SUCCESS = '[Appointments] Mark Appointment Missed Success';

export const ADD_APPOINTMENT = '[Appointments] Add Appointment';
export const ADD_APPOINTMENT_SUCCESS = '[Appointments] Add Appointment Success';
export const ADD_APPOINTMENT_FAIL = '[Appointments] Add Appointment Fail';


export class LoadAppointments implements Action {
    readonly type = LOAD_APPOINTMENTS;
    constructor(public payload: GetClinicAppointmentsQuery) {
    }
}

export class LoadAppointmentsFail implements Action {
    readonly type = LOAD_APPOINTMENTS_FAIL;

    constructor(public payload: any) {}

}

export class LoadAppointmentsSuccess implements Action {
    readonly type = LOAD_APPOINTMENTS_SUCCESS;

    constructor(public payload: Appointment[]) {}
}

export class MarkAppointmentCompleted implements Action {
    readonly type = MARK_APPOINTMENT_COMPLETED;

    constructor(public payload: MarkAppointmentCompletedCommand) {}
}

export class MarkAppointmentCompletedFail implements Action {
    readonly type = MARK_APPOINTMENT_COMPLETED_FAIL;

    constructor(public payload: any) {}
}

export class MarkAppointmentCompletedSuccess implements Action {
    readonly type = MARK_APPOINTMENT_COMPLETED_SUCCESS;
    
    constructor(public payload: MarkAppointmentCompletedCommand) {}
}

export class MarkAppointmentMissed implements Action {
    readonly type = MARK_APPOINTMENT_MISSED;

    constructor(public payload: MarkAppointmentMissedCommand) {}
}

export class MarkAppointmentMissedFail implements Action {
    readonly type = MARK_APPOINTMENT_MISSED_FAIL;

    constructor(public payload: any) {}
}

export class MarkAppointmentMissedSuccess implements Action {
    readonly type = MARK_APPOINTMENT_MISSED_SUCCESS;
    
    constructor(public payload: MarkAppointmentMissedCommand) {}
}

export class AddAppoinement implements Action {
    readonly type = ADD_APPOINTMENT;

    constructor(public payload: AddAppointmentCommand) {}
}

export class AddAppoinementFail implements Action {
    readonly type = ADD_APPOINTMENT_FAIL;

    constructor(public payload: any) {}
}

export class AddAppoinementSuccess implements Action {
    readonly type = ADD_APPOINTMENT_SUCCESS;

    constructor(public payload: Appointment) {}
}

// action types
export type AppoinmentActions = 
| LoadAppointments
| LoadAppointmentsFail 
| LoadAppointmentsSuccess
| MarkAppointmentCompleted
| MarkAppointmentCompletedFail
| MarkAppointmentCompletedSuccess
| MarkAppointmentMissed
| MarkAppointmentMissedFail
| MarkAppointmentMissedSuccess
| AddAppoinement
| AddAppoinementFail
| AddAppoinementSuccess;
