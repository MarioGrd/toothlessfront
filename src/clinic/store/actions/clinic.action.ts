import { Action } from '@ngrx/store';
import { Clinic } from '../../models/clinic.model';
import { AddClinicProcedureCommand } from '../../models/commands/addClinicProcedureCommand.model';

export const LOAD_CLINIC = '[Clinic] Load Clinic';
export const LOAD_CLINIC_FAIL = '[Clinic] Load Clinic Fail';
export const LOAD_CLINIC_SUCCESS = '[Clinic] Load Clinic Success';

export const ADD_CLINIC_PROCEDURE = '[Clinic] Add Clinic Procedure';
export const ADD_CLINIC_PROCEDURE_FAIL = '[Clinic] Add Clinic Procedure Fail';
export const ADD_CLINIC_PROCEDURE_SUCCESS = '[Clinic] Add Clinic Procedure Success';


export class LoadClinic implements Action {
    readonly type = LOAD_CLINIC;
    constructor(public payload: string) {
    }
}

export class LoadClinicFail implements Action {
    readonly type = LOAD_CLINIC_FAIL;

    constructor(public payload: any) {}

}

export class LoadClinicSuccess implements Action {
    readonly type = LOAD_CLINIC_SUCCESS;

    constructor(public payload: Clinic) {}
}

export class AddClinicProcedure implements Action {
    readonly type = ADD_CLINIC_PROCEDURE;

    constructor(public payload: AddClinicProcedureCommand) {}
}

export class AddClinicProcedureSuccess implements Action {
    readonly type = ADD_CLINIC_PROCEDURE_SUCCESS;

    constructor (public payload: any) {}
}

export class AddClinicProcedureFail implements Action {
    readonly type = ADD_CLINIC_PROCEDURE_FAIL;

    constructor (public payload: any) {}
}


// action types
export type ClinicActions = 
    | LoadClinic
    | LoadClinicFail 
    | LoadClinicSuccess
    | AddClinicProcedure
    | AddClinicProcedureFail
    | AddClinicProcedureSuccess 

