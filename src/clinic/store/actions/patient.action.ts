import { Action } from '@ngrx/store';
import { Patient } from '../../models/patient.model';
import { Paged } from '../../models/paged.model';
import { GetPatientsQuery } from '../../models/queries/getPatientsQuery.model';
import { AddPatientCommand } from '../../models/commands/addPatientCommand.model';

export const LOAD_PATIENTS = '[Patients] Load Patients';
export const LOAD_PATIENTS_FAIL = '[Patients] Load Patients Fail';
export const LOAD_PATIENTS_SUCCESS = '[Patients] Load Patients Success';

export const ADD_PATIENT = '[Patients] Add Patient';
export const ADD_PATIENT_FAIL = '[Patients] Add Patient Fail';
export const ADD_PATIENT_SUCCESS = '[Patients] Add Patient Success';


export class LoadPatients implements Action {
    readonly type = LOAD_PATIENTS;
    constructor(public payload: GetPatientsQuery) {
    }
}

export class LoadPatientsFail implements Action {
    readonly type = LOAD_PATIENTS_FAIL;

    constructor(public payload: any) {}

}

export class LoadPatientsSuccess implements Action {
    readonly type = LOAD_PATIENTS_SUCCESS;

    constructor(public payload: Paged<Patient>) {}
}

export class AddPatient implements Action {
    readonly type = ADD_PATIENT;

    constructor(public payload: AddPatientCommand ) {}
}

export class AddPatientSuccess implements Action {
    readonly type = ADD_PATIENT_SUCCESS; 

    constructor(public payload: any) {}
}

export class AddPatientFail implements Action {
    readonly type = ADD_PATIENT_FAIL;

    constructor(public payload: any) {}
}

// action types
export type PatientActions = 
    | LoadPatients
    | LoadPatientsFail 
    | LoadPatientsSuccess
    | AddPatient
    | AddPatientFail
    | AddPatientSuccess; 
