import { Action } from '@ngrx/store';
import { Report } from '../../models/report.model';

import { GetReportQuery } from '../../models/queries/GetReportQuery.model';


export const LOAD_REPORTS = '[Reports] Load Reports';
export const LOAD_REPORTS_FAIL = '[Reports] Load Reports Fail';
export const LOAD_REPORTS_SUCCESS = '[Reports] Load Reports Success';

export class LoadReports implements Action {
    readonly type = LOAD_REPORTS;
    constructor(public payload: GetReportQuery) {
    }
}

export class LoadReportsFail implements Action {
    readonly type = LOAD_REPORTS_FAIL;

    constructor(public payload: any) {}

}

export class LoadReportsSuccess implements Action {
    readonly type = LOAD_REPORTS_SUCCESS;

    constructor(public payload: Report) {}
}

// action types
export type ReportActions = 
| LoadReports
| LoadReportsFail 
| LoadReportsSuccess