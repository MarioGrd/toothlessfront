import { Appointment } from '../../models/appointment.model'
import * as fromAppointment from '../actions/appointment.action';

import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export interface AppointmentState {
    entities: { [id: string] : Appointment };
    loaded: boolean,
    loading: boolean,
}

export const initialState : AppointmentState = {
    entities: null,
    loading: false,
    loaded: false,
}

export function reducer(
    state = initialState, 
    action: fromAppointment.AppoinmentActions
): AppointmentState {

    switch(action.type) {
        
        case fromAppointment.LOAD_APPOINTMENTS: {
           return {
                ...state,
                loading: true
            };
        }

        case fromAppointment.LOAD_APPOINTMENTS_SUCCESS: {

            let appointments = action.payload;
            const entities = appointments.reduce(
                (entities : { [appointmentId: string]: Appointment }, appointment: Appointment) => {

                    return {
                        ...entities, 
                        [appointment.id]: appointment,
                    }
                }, {}) 

            return {
                ...state,
                entities,
                loaded: true,
                loading: false,
            }

        }

        case fromAppointment.LOAD_APPOINTMENTS_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            };
        }

        case fromAppointment.MARK_APPOINTMENT_COMPLETED: {
            return {
                ...state,
                loading: true,
                loaded: false,
            }
        }

        case fromAppointment.MARK_APPOINTMENT_COMPLETED_SUCCESS: {
            const appointment = state.entities[action.payload.appointmentId];

            const copy = Object.assign({}, appointment);
            copy.appointmentStatusId = 2;

            const entities = {
                ...state.entities,
                [copy.id]: copy
            }

            return {
                ...state,
                loading: false,
                loaded: false,
                entities
            }
        }

        case fromAppointment.MARK_APPOINTMENT_COMPLETED_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            }
        }

        case fromAppointment.MARK_APPOINTMENT_MISSED: {
            return {
                ...state,
                loading: true,
                loaded: false,
            }
        }

        case fromAppointment.MARK_APPOINTMENT_MISSED_SUCCESS: {
            const appointment = state.entities[action.payload.appointmentId];

            const copy = Object.assign({}, appointment);
            copy.appointmentStatusId = 3;

            const entities = {
                ...state.entities,
                [copy.id]: copy
            }

            return {
                ...state,
                loading: false,
                loaded: false,
                entities
            }
        }

        case fromAppointment.MARK_APPOINTMENT_MISSED_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            }
        }

        case fromAppointment.ADD_APPOINTMENT: {
            return {
                ...state,
                loading: true,
                loaded: false,
            }
        }

        case fromAppointment.ADD_APPOINTMENT_SUCCESS: {
            const appointment = action.payload;

            const entities = {
                ...state.entities,
                [appointment.id]: appointment
            }

            return {
                ...state,
                loading: false,
                loaded: false,
                entities
            }
        }

        case fromAppointment.ADD_APPOINTMENT_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            }
        }
    }

    return state;
}

export const getAppoinmentsLoaded = (state: AppointmentState) => state.loaded;
export const getAppoinmentsLoading = (state: AppointmentState) => state.loading;
export const getAppoinmentsEntities = (state: AppointmentState) => state.entities;