import { Patient } from '../../models/patient.model'
import * as fromPatients from '../actions/patient.action';

import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { ENTER_CLASSNAME } from '@angular/animations/browser/src/util';

export interface PatientsState {
    entities: { [patientId: string] : Patient };
    loaded: boolean,
    loading: boolean,
    pageNumber: number,
    pageSize: number,
    count: number
}

export const initialState: PatientsState = {
    entities: {},
    loaded: false,
    loading: false,
    pageNumber: 1,
    pageSize: 10,
    count: 0
  };

export function reducer(
    state = initialState, 
    action: fromPatients.PatientActions
): PatientsState {

    switch(action.type) {
        
        case fromPatients.LOAD_PATIENTS: {
           return {
                ...state,
                loading: true
            };
        }

        case fromPatients.LOAD_PATIENTS_SUCCESS: {

            let patients = action.payload.data;
            
            const entities = patients.reduce(
                (entities : { [patientId: string]: Patient }, patient: Patient) => {

                    return {
                        ...entities, 
                        [patient.id]: patient,
                    }
                }, {}) 

            const pageNumber = action.payload.pageNumber;
            const pageSize = action.payload.pageSize;
            const count = action.payload.count;

            return {
                ...state,
                entities,
                loaded: true,
                loading: false,
                pageNumber: pageNumber,
                pageSize: pageSize,
                count: count
            }

        }

        case fromPatients.LOAD_PATIENTS_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            };
        }

        case fromPatients.ADD_PATIENT: {
            return {
                ...state,
                loading: true,
                loaded: false,
                pageNumber: 1
            }
        }

        case fromPatients.ADD_PATIENT_SUCCESS: {

            const patient = action.payload;
            const entities = {
                ...state.entities,
                [patient.id]: patient
            } 

            return {
                ...state,
                entities,
                loading: false,
                loaded: true
            }
        }

        case fromPatients.ADD_PATIENT_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            };
        }
    }

    return state;
}


export const getPatientsLoaded = (state: PatientsState) => state.loaded;
export const getPatientsLoading = (state: PatientsState) => state.loading;
export const getPatientsEntities = (state: PatientsState) => state.entities;
export const getPatientsPageNumber = (state: PatientsState) => state.pageNumber;
export const getPatientsCount = (state: PatientsState) => state.count;