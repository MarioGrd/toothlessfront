import { Clinic } from '../../models/clinic.model'
import * as fromClinic from '../actions/clinic.action';

import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export interface ClinicState {
    entity : Clinic,
    loaded: boolean,
    loading: boolean,
}

export const initialState : ClinicState = {
    entity: null,
    loading: false,
    loaded: false,
}

export function reducer(
    state = initialState, 
    action: fromClinic.ClinicActions
): ClinicState {

    switch(action.type) {
        
        case fromClinic.LOAD_CLINIC: {
           return {
                ...state,
                loading: true
            };
        }

        case fromClinic.LOAD_CLINIC_SUCCESS: {

            const entity = action.payload;
            return {
                ...state,
                entity,
                loading:false,
                loaded: true
            }

        }

        case fromClinic.LOAD_CLINIC_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            };
        }

        case fromClinic.ADD_CLINIC_PROCEDURE: {
            return {
                ...state,
                loading: true,
                loaded: false
            }
        }

        case fromClinic.ADD_CLINIC_PROCEDURE_SUCCESS: {

            const procedures = [...state.entity.procedures, action.payload ];
            const  clinic = {...state.entity, procedures};
            return { 
                ...state,
                entity: clinic, 
                loaded: true, 
                loading: false };
        }

        case fromClinic.ADD_CLINIC_PROCEDURE_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            }
        }
    }

    return state;
}

export const getClinicLoaded = (state: ClinicState) => state.loaded;
export const getClinicLoading = (state: ClinicState) => state.loading;
export const getClinicEntity = (state: ClinicState) => state.entity;
export const getClinicProcedures = (state: ClinicState) => state.entity.procedures;
export const getClinicId = (state: ClinicState) => state.entity.id;
