import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromClinic from './clinic.reducer';
import * as fromPatient from './patient.reducer';
import * as fromAppointment from './appointment.reducer';
import * as fromReport from './report.reducer';

export interface OrdinationState {
    clinic: fromClinic.ClinicState,
    patients: fromPatient.PatientsState,
    appointments: fromAppointment.AppointmentState,
    report: fromReport.ReportState
};

export const reducers : ActionReducerMap<OrdinationState> = {
    clinic: fromClinic.reducer,
    patients: fromPatient.reducer,
    appointments: fromAppointment.reducer,
    report: fromReport.reducer
};

export const getOrdinationState = createFeatureSelector<OrdinationState>(
    'ordination'
);
