import { ReportItem } from '../../models/report.item.model'
import { ReportData } from '../../models/report.data.model'
import * as fromReport from '../actions/report.action';

import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export interface ReportState {
    entities : ReportItem[],
    info: ReportData,
    loaded: boolean,
    loading: boolean,
}

export const initialState : ReportState = {
    entities: [],
    info: null,
    loading: false,
    loaded: false,
}

export function reducer(
    state = initialState, 
    action: fromReport.ReportActions
): ReportState {

    switch(action.type) {
        
        case fromReport.LOAD_REPORTS: {
           return {
                ...state,
                loading: true
            };
        }

        case fromReport.LOAD_REPORTS_SUCCESS: {

            const entities = action.payload.items;
            const data = action.payload.data;

            return {
                ...state,
                entities: entities,
                info: data,
                loading:false,
                loaded: true
            }

        }

        case fromReport.LOAD_REPORTS_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false
            };
        }
    }

    return state;
}

export const getReportLoaded = (state: ReportState) => state.loaded;
export const getReportLoading = (state: ReportState) => state.loading;
export const getReportEntities = (state: ReportState) => state.entities;
export const getReportData = (state: ReportState) => state.info;
