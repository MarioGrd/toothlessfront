import { Injectable } from "@angular/core";
import { Effect, Actions } from "@ngrx/effects";

import { switchMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";

import * as fromRoot from "../../../app/store";
import * as appointmentActions from "../actions/appointment.action";
import * as fromServices from "../../services";
import { debounceTime } from "rxjs/internal/operators/debounceTime";
import { debounce } from "rxjs/internal/operators/debounce";

@Injectable()
export class AppointmentEffects {
  constructor(
    private actions$: Actions,
    private appointmentService: fromServices.AppointmentService
  ) {}

  @Effect()
  loadAppointments$ = this.actions$.ofType(appointmentActions.LOAD_APPOINTMENTS).pipe(
    switchMap((action: appointmentActions.LoadAppointments) => {
      return this.appointmentService.getAppointments(action.payload).pipe(
        map(data => new appointmentActions.LoadAppointmentsSuccess(data)),
        catchError(error => of(new appointmentActions.LoadAppointmentsFail(error)))
      );
    })
  );

  @Effect()
  markAppointmentCompleted$ = this.actions$.ofType(appointmentActions.MARK_APPOINTMENT_COMPLETED).pipe(
    switchMap((action: appointmentActions.MarkAppointmentCompleted) => {
      return this.appointmentService.markAppointmentCompleted(action.payload).pipe(
        map(data => new appointmentActions.MarkAppointmentCompletedSuccess(action.payload)),
        catchError(error => of(new appointmentActions.MarkAppointmentCompletedFail(error)))
      );
    })
  );

  @Effect()
  markAppointmentMissed$ = this.actions$.ofType(appointmentActions.MARK_APPOINTMENT_MISSED).pipe(
    switchMap((action: appointmentActions.MarkAppointmentMissed) => {
      return this.appointmentService.markAppointmentMissed(action.payload).pipe(
        map(data => new appointmentActions.MarkAppointmentMissedSuccess(action.payload)),
        catchError(error => of(new appointmentActions.MarkAppointmentMissedFail(error)))
      );
    })
  );

  @Effect()
  addAppointment$ = this.actions$.ofType(appointmentActions.ADD_APPOINTMENT).pipe(
    switchMap((action: appointmentActions.AddAppoinement) => {
      return this.appointmentService.addAppointment(action.payload).pipe(
        map(data => new appointmentActions.AddAppoinementSuccess(data)),
        catchError(error => of(new appointmentActions.AddAppoinementFail(error)))
      );
    })
  );
}