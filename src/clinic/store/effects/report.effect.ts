import { Injectable } from "@angular/core";
import { Effect, Actions } from "@ngrx/effects";

import { switchMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";

import * as fromRoot from "../../../app/store";
import * as reportActions from "../actions/report.action";
import * as fromServices from "../../services";
import { debounceTime } from "rxjs/internal/operators/debounceTime";
import { debounce } from "rxjs/internal/operators/debounce";

@Injectable()
export class ReportEffects {
  constructor(
    private actions$: Actions,
    private reportService: fromServices.ReportService
  ) {}

  @Effect()
  loadReport$ = this.actions$.ofType(reportActions.LOAD_REPORTS).pipe(
    switchMap((action: reportActions.LoadReports) => {
      return this.reportService.getReports(action.payload).pipe(
        map(data => new reportActions.LoadReportsSuccess(data)),
        catchError(error => of(new reportActions.LoadReportsFail(error)))
      );
    })
  );
}