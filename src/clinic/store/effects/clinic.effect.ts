import { Injectable } from "@angular/core";
import { Effect, Actions } from "@ngrx/effects";

import { switchMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";

import * as fromRoot from "../../../app/store";
import * as clinicActions from "../actions/clinic.action";
import * as fromServices from "../../services";
import { debounceTime } from "rxjs/internal/operators/debounceTime";
import { debounce } from "rxjs/internal/operators/debounce";

@Injectable()
export class ClinicEffects {
  constructor(
    private actions$: Actions,
    private clinicService: fromServices.ClinicService
  ) {}

  @Effect()
  loadClinic$ = this.actions$.ofType(clinicActions.LOAD_CLINIC).pipe(
    switchMap((action: clinicActions.LoadClinic) => {
      return this.clinicService.getClinic(action.payload).pipe(
        map(data => new clinicActions.LoadClinicSuccess(data)),
        catchError(error => of(new clinicActions.LoadClinicFail(error)))
      );
    })
  );

  @Effect()
  addProcedure$ = this.actions$.ofType(clinicActions.ADD_CLINIC_PROCEDURE).pipe(
    switchMap((action: clinicActions.AddClinicProcedure) => {
      return this.clinicService.addProcedure(action.payload).pipe(
        map(data => new clinicActions.AddClinicProcedureSuccess(data)),
        catchError(error => of(new clinicActions.AddClinicProcedureFail(error)))
      );
    })
  );
}
