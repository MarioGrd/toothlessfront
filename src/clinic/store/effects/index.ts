import { ClinicEffects } from './clinic.effect';
import { PatientEffects } from './patient.effect';
import { AppointmentEffects} from './appointment.effect';
import { ReportEffects } from './report.effect';

export const effects: any[] = [ClinicEffects, PatientEffects, AppointmentEffects, ReportEffects]; 

export * from './clinic.effect';
export * from './patient.effect';
export * from'./appointment.effect';
export * from './report.effect';