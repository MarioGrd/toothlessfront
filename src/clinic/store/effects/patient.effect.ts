import { Injectable } from "@angular/core";
import { Effect, Actions } from "@ngrx/effects";

import { switchMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";

import * as fromRoot from "../../../app/store";
import * as patientActions from "../actions/patient.action";
import * as fromServices from "../../services";
import { debounceTime } from "rxjs/internal/operators/debounceTime";
import { debounce } from "rxjs/internal/operators/debounce";

@Injectable()
export class PatientEffects {
  constructor(
    private actions$: Actions,
    private patientService: fromServices.PatientService
  ) {}

  @Effect()
  loadPatients$ = this.actions$.ofType(patientActions.LOAD_PATIENTS).pipe(
    switchMap((action: patientActions.LoadPatients) => {
      return this.patientService.getPatients(action.payload).pipe(
        map(data => new patientActions.LoadPatientsSuccess(data)),
        catchError(error => of(new patientActions.LoadPatientsFail(error)))
      );
    })
  );

  @Effect()
  addpatient$ = this.actions$.ofType(patientActions.ADD_PATIENT).pipe(
    switchMap((action: patientActions.AddPatient) => {
      return this.patientService.addPatient(action.payload).pipe(
        map(data => new patientActions.AddPatientSuccess(data)),
        catchError(error => of(new patientActions.AddPatientFail(error)))
      );
    })
  );
}
