import { createSelector } from '@ngrx/store';

import * as fromRoot from '../../../app/store';
import * as fromFeature from '../reducers';
import * as fromClinics from '../reducers/clinic.reducer';

import { Clinic } from '../../models/clinic.model';

export const getClinicState = createSelector(
    fromFeature.getOrdinationState,
    (state : fromFeature.OrdinationState) => state.clinic
)

export const getClinic = createSelector(
    getClinicState,
    fromClinics.getClinicEntity
)

export const getClinicId = createSelector(
    getClinicState,
    fromClinics.getClinicId
)

export const getClinicLoaded = createSelector(
    getClinicState,
    fromClinics.getClinicLoaded
)

export const getClinicLoading = createSelector(
    getClinicState,
    fromClinics.getClinicLoading
)
