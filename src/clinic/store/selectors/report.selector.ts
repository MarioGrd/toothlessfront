import { createSelector } from '@ngrx/store';

import * as fromRoot from '../../../app/store';
import * as fromFeature from '../reducers';
import * as fromReports from '../reducers/report.reducer';

export const getReportState = createSelector(
    fromFeature.getOrdinationState,
    (state : fromFeature.OrdinationState) => state.report
)

export const getReportEntities = createSelector(
    getReportState,
    fromReports.getReportEntities
)

export const getReportData = createSelector(
    getReportState,
    fromReports.getReportData
)

export const getReportLoaded = createSelector(
    getReportState,
    fromReports.getReportLoaded
)

export const getReportLoading = createSelector(
    getReportState,
    fromReports.getReportLoading
)
