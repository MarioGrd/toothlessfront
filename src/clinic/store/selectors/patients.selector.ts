import { createSelector } from '@ngrx/store';

import * as fromRoot from '../../../app/store';
import * as fromFeature from '../reducers';
import * as fromPatients from '../reducers/patient.reducer';

export const getPatientsState = createSelector(
    fromFeature.getOrdinationState,
    (state : fromFeature.OrdinationState) => state.patients
)

export const getPatientsEntities = createSelector(
    getPatientsState,
    fromPatients.getPatientsEntities
)

export const getAllPatients = createSelector(
    getPatientsEntities,
    (entities) => {
        return Object.keys(entities).map(patientId => entities[patientId]);
    }
)

export const getPatientsLoaded = createSelector(
    getPatientsState,
    fromPatients.getPatientsLoaded
)

export const getPatientsLoading = createSelector(
    getPatientsState,
    fromPatients.getPatientsLoading
)

export const getPatientsPageNumber = createSelector(
    getPatientsState,
    fromPatients.getPatientsPageNumber
)

export const getPatientsCount = createSelector(
    getPatientsState,
    fromPatients.getPatientsCount
)