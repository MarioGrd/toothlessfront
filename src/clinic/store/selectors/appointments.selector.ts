import { createSelector } from '@ngrx/store';

import * as fromRoot from '../../../app/store';
import * as fromFeature from '../reducers';
import * as fromAppointment from '../reducers/appointment.reducer';

import { Appointment } from '../../models/appointment.model';

export const getAppointmentsState = createSelector(
    fromFeature.getOrdinationState,
    (state : fromFeature.OrdinationState) => state.appointments
)

export const getAppointmentsEntities = createSelector(
    getAppointmentsState,
    fromAppointment.getAppoinmentsEntities
)

export const getAllAppointments = createSelector(
    getAppointmentsEntities,
    (entities) => {
        return Object.keys(entities).map(appointmentId => entities[appointmentId]);
    }
)

export const getAppoinmentsLoaded = createSelector(
    getAppointmentsState,
    fromAppointment.getAppoinmentsLoaded
)

export const getAppoinmentsLoading = createSelector(
    getAppointmentsState,
    fromAppointment.getAppoinmentsLoading
)
