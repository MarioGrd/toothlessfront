import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Clinic } from '../../models/clinic.model';
import * as fromStore from '../../store';

@Component({
  selector: 'clinic-details',
  templateUrl: './clinic.details.component.html'
})
export class ClinicDetailsComponent implements OnInit {

    clinic: Clinic;
  
    constructor(private store: Store<fromStore.OrdinationState>) { }
  
    ngOnInit() {

      this.store.select(fromStore.getClinic).subscribe((clinic) =>{
        this.clinic = clinic;
      });
      
    }
}
