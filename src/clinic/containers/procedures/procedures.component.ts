import {
  Component,
  OnInit,
  Input,
  ViewChild,
  TemplateRef
} from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { Clinic } from "../../models/clinic.model";
import { Procedure } from "../../models/procedure.model";
import * as fromStore from "../../store";
import * as fromServices from "../../services";
import * as fromComponents from "../../components";


import {MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: "procedures",
  templateUrl: "./procedures.component.html"
})
export class ProceduresComponent implements OnInit {
  clinic$: Observable<Clinic>;
  clinic: Clinic;
  closeResult: string;

  constructor(
    public dialog: MatDialog,
    private store: Store<fromStore.OrdinationState>,
    private clinicService: fromServices.ClinicService
  ) {}

  ngOnInit() {
    this.store.select(fromStore.getClinic).subscribe(data => {
      this.clinic = data;
    });
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(fromComponents.ProcedureFormComponent, {width: '50%'})
  }
}
