import { ClinicComponent} from './main/clinic.component';
import { ClinicDetailsComponent } from './details/clinic.details.component';
import { PatientsComponent } from './patients/patients.component';
import { ProceduresComponent } from './procedures/procedures.component';
import { AppointmentsComponent } from './appointments/appointments.component'
import { ReportsComponent } from './reports/reports.component';


export const containers: any[] = [ClinicComponent,ClinicDetailsComponent, PatientsComponent, ProceduresComponent, AppointmentsComponent, ReportsComponent];

export * from './main/clinic.component';
export * from './details/clinic.details.component';
export * from './patients/patients.component';
export * from './procedures/procedures.component';
export * from './appointments/appointments.component';
export * from './reports/reports.component';