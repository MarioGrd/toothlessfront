import {
  startOfDay,
  endOfDay,
  startOfWeek,
  endOfWeek,
  startOfMonth,
  endOfMonth,
  isSameMonth,
  isSameDay,
  isSameHour
} from "date-fns";

import { Component, OnInit } from "@angular/core";
import {
  FormControl,
  Validators,
  FormBuilder,
  FormGroup
} from "@angular/forms";

import { Subject } from "rxjs";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap/modal/modal.module";
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent
} from "angular-calendar";

import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import * as fromStore from "../../store";
import * as fromServices from "../../services";

import { Clinic } from "../../models/clinic.model";
import { GetReportQuery } from "../../models/queries/GetReportQuery.model";
import { SearchClinicProcedureQuery } from "../../models/queries/SearchClinicProcedureQuery.model";
import { ReportItem } from "../../models/report.item.model";
import { ReportData } from "../../models/report.data.model";
import { Procedure } from "../../models/procedure.model";
import { ColorStatus } from "../../models/enums/color-status.model";
import { AppointmentStatus } from "../../models/enums/appointment-status.model";

import { ReportStatus } from "../../models/enums/report-status.model";

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

import { NgxChartsModule } from "@swimlane/ngx-charts";

import {
  COLOR_STATUS,
  APPOINTMENT_STATUS,
  REPORT_STATUS
} from "../../models/constants/constants";

@Component({
  selector: "reports",
  templateUrl: "./reports.component.html"
})
export class ReportsComponent {
  clinic: Clinic;
  reportItems$: Observable<ReportItem[]>;
  procedures$: Observable<Procedure[]>;

  query: GetReportQuery;
  reports$: Observable<ReportItem[]>;
  reportInfo: ReportData;
  events: CalendarEvent[];

  view: string = "week";
  viewDate: Date = new Date();
  refresh: Subject<any> = new Subject();

  colorStatus: ColorStatus[] = COLOR_STATUS;
  reportStatus: ReportStatus[] = REPORT_STATUS;

  activeDayIsOpen: boolean = true;

  hours: any[];
  hoursCount: any[];
  size = [400, 200];
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showYAxisLabel = true;
  yAxisLabel = "Hours";
  yAxisLabelCount = "Count"

  colorScheme = {
    domain: [COLOR_STATUS[1].primary, COLOR_STATUS[2].primary,COLOR_STATUS[0].primary]
  };


  procedure: FormControl;
  getProcedureError() {
    if (this.procedure.hasError("required")) {
      return "Procedure is required.";
    }
  }

  displayProcedureFn(item): string | undefined {
    return item !== null ? item.name : "";
  }

  constructor(
    private store: Store<fromStore.OrdinationState>,
    private clinicService: fromServices.ClinicService
  ) {



    this.store.select(fromStore.getClinic).subscribe(clinic => {
      this.clinic = clinic;
    });

    this.procedure = new FormControl();

    this.query = {
      ClinicId: this.clinic.id,
      StartTime: startOfWeek(this.viewDate).toUTCString(),
      EndTime: endOfWeek(this.viewDate).toUTCString(),
      ProcedureId: null
    };

    this.procedures$ = this.procedure.valueChanges.pipe(
      switchMap(value => {
        let query: SearchClinicProcedureQuery = {
          clinicId: this.clinic.id,
          name: value
        };
        return this.clinicService.searchProcedures(query);
      })
    );

    this.store.select(fromStore.getReportState).subscribe(data => {
      this.reportInfo = data.info;

      this.hours = [
        {
          "name": "Completed",
          "value": data.info.completedTotalTime
        },
        {
          "name": "Missed",
          "value": data.info.missedTotalTime
        },
        {
          "name": "Unused",
          "value": data.info.unusedTotalTime
        }
      ];

      this.hoursCount = [{
            "name": "Completed",
            "value": data.info.completedCount
          },
          {
            "name": "Missed",
            "value": data.info.missedCount
          },
          {
            "name": "Unused",
            "value": data.info.unusedCount
          }
        ];

    
      this.events = data.entities.map(item => {
        let title = this.reportStatus.find(
          status => status.id === item.appointmentStatusId
        ).value;
        let from = "FROM: " + new Date(item.startDate).toDateString();
        let to = "TO: " + new Date(item.endDate).toDateString();

        return {
          start: new Date(item.startDate),
          end: new Date(item.endDate),
          title: title + " | " + from + " --- " + to,
          color: this.colorStatus.find(
            color => color.value === item.appointmentStatusId
          )
        };
      });
      this.refresh.next();
    });
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.refresh.next();
  }

  onSearch(): void {
    this.searchReports();
  }

  onSearchClear(): void {
    this.procedure.setValue(null);
      this.searchReports();
  }

  onDateChange(view): void {
    if (view) this.view = view;

    this.activeDayIsOpen = false;
    this.searchReports();
  }

  startDate = startOfWeek(new Date());
  endDate = startOfWeek(new Date());

  searchReports(): void {
    let procedureId = null;
    let startDate;
    let endDate;

    if (this.procedure.value && this.procedure.value.id) {
      procedureId = this.procedure.value.id;
    }

    if (this.view === "month") {
      this.startDate = startOfMonth(this.viewDate);
      this.endDate = endOfMonth(this.viewDate);
    } else if (this.view === "week") {
      this.startDate = startOfWeek(this.viewDate);
      this.endDate = endOfWeek(this.viewDate);
    } else {
      this.startDate = startOfDay(this.viewDate);
      this.endDate = endOfDay(this.viewDate);
    }

    let query: GetReportQuery = {
      ClinicId: this.clinic.id,
      ProcedureId: procedureId,
      StartTime: this.startDate.toUTCString(),
      EndTime: this.endDate.toUTCString()
    };

    this.store.dispatch(new fromStore.LoadReports(query));
  }
}
