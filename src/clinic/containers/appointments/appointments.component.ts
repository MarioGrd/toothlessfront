import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  OnInit
} from "@angular/core";

import { FormControl, FormGroup, Validators } from "@angular/forms";

import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import * as fromStore from "../../store";
import { map, switchMap } from "rxjs/operators";

import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  startOfMonth,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
  addMinutes,
  startOfWeek,
  endOfWeek
} from "date-fns";

import { Subject } from "rxjs";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap/modal/modal.module";
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent
} from "angular-calendar";

import { Appointment } from "../../models/appointment.model";
import { Clinic } from "../../models/clinic.model";
import { Patient } from "../../models/patient.model";
import { Procedure } from "../../models/procedure.model";
import { ColorStatus } from "../../models/enums/color-status.model";
import { AppointmentStatus } from "../../models/enums/appointment-status.model";
import { GetClinicAppointmentsQuery } from "../../models/queries/GetClinicAppointmentsQuery.model";
import { SearchClinicPatientQuery } from "../../models/queries/SearchClinicPatientQuery.model";
import { SearchClinicProcedureQuery } from "../../models/queries/SearchClinicProcedureQuery.model";

import { MarkAppointmentCompletedCommand } from "../../models/commands/MarkAppointmentCompletedCommand.model";
import { MarkAppointmentMissedCommand } from "../../models/commands/MarkAppointmentMissedCommand.model";
import { AddAppointmentCommand } from "../../models/commands/AddAppointmentCommand.model";

import { COLOR_STATUS, APPOINTMENT_STATUS } from '../../models/constants/constants';
import * as fromServices from "../../services";

@Component({
  selector: "appointments",
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: "appointments.component.html"
})
export class AppointmentsComponent implements OnInit {
  @ViewChild("modalContent") modalContent: TemplateRef<any>;

  colorStatus: ColorStatus[] = COLOR_STATUS;
  appointmentStatus: AppointmentStatus[] = APPOINTMENT_STATUS;

  patients$: Observable<Patient[]>;
  procedures$: Observable<Procedure[]>;
  clinic: Clinic;

  command: AddAppointmentCommand;

  patient: FormControl;
  procedure: FormControl;
  startTime: FormControl;
  commandForm: FormGroup;

  getPatientError() {
    if (this.patient.hasError("required")) {
      return "Patient is required.";
    }
  }

  getProcedureError() {
    if (this.procedure.hasError("required")) {
      return "Procedure is required.";
    }
  }

  getStartTimeError() {
    if (this.startTime.hasError("required")) {
      return "Time is required.";
    }
  }

  events: CalendarEvent[];
  isInvalidRange: boolean = false;
  reportMsg: string;

  submit() {

    if (this.commandForm.invalid) {
        return;
    }

    let formDate = this.startTime.value;

    let procedureMinutes = this.procedure.value.minutes;
    let day = this.viewDate.getDate();
    let month = this.viewDate.getMonth();
    let year = this.viewDate.getFullYear();
    let hour = formDate.hour;
    let minute = formDate.minute;
    let startDate = new Date(year, month, day, hour, minute)
    let endDate = addMinutes(startDate, procedureMinutes);

    if (startDate.getDay() === 6 || startDate.getDay() === 0) {
        this.reportMsg = "Don't work on weekends.";
        this.isInvalidRange = true;
        return;
    }

    let minutesStart = hour * 60 + minute;
    let minutesEnd = minutesStart + procedureMinutes;

    let clinicMinutesStart = (this.clinic.workTime.startHour * 60) + this.clinic.workTime.startMinute;
    let clinicMinutesEnd = (this.clinic.workTime.endHour * 60) + this.clinic.workTime.endMinute; 

    this.isInvalidRange = false;
    
    if (minutesStart < clinicMinutesStart || minutesStart > clinicMinutesEnd || minutesEnd < clinicMinutesStart || minutesEnd > clinicMinutesEnd) {
        this.reportMsg = "Clinic doesn't work in selected time range.";
        this.isInvalidRange = true;
        return;
    }

    for (var i = 0; i < this.events.length; i++) {

        if (startDate >= this.events[i].start && startDate <= this.events[i].end) {
            this.reportMsg = "Selected range is already filled.";
            this.isInvalidRange = true;
        }

        if (endDate >= this.events[i].start && endDate <= this.events[i].end){
            this.reportMsg = "Selected range is already filled.";
            this.isInvalidRange = true;
        }
    }

    if (this.isInvalidRange) {
        return;
    }

    this.command = {
        clinicId: this.clinic.id,
        startTime: startDate.toLocaleString(),
        procedureId: this.procedure.value.id,
        patientId: this.patient.value.id
    }
    
    this.store.dispatch(new fromStore.AddAppoinement(this.command));
  }

  displayPatientFn(item): string | undefined {
    return item !== null ? item.fullName : "";
  }

  displayProcedureFn(item): string | undefined {
    return item !== null ? item.name : "";
  }

  ngOnInit() {

    this.store.select(fromStore.getClinic).subscribe(data => {
        this.clinic = data;
    });

    this.patient = new FormControl('', [
      Validators.required
    ]);
    this.procedure = new FormControl('', [
      Validators.required
    ]);
    this.startTime = new FormControl('', [
      Validators.required
    ]);

    this.commandForm = new FormGroup({
      patient: this.patient,
      procedure: this.procedure,
      startTime: this.startTime
    });

    this.procedures$ = this.procedure.valueChanges.pipe(
      switchMap(value => {
        let query: SearchClinicProcedureQuery = {
          clinicId: this.clinic.id,
          name: value
        };
        return this.clinicService.searchProcedures(query);
      })
    );

    this.patients$ = this.patient.valueChanges.pipe(
      switchMap(value => {
        let query: SearchClinicPatientQuery = {
          clinicId: this.clinic.id,
          lastName: value
        };
        return this.patientService.searchPatients(query);
      })
    );

    this.store.select(fromStore.getAllAppointments).subscribe(data => {
      this.refresh.next();

      this.events = data.map(appointment => {
        return {
          start: new Date(appointment.startDate),
          end: new Date(appointment.endDate),
          title: appointment.patient,
          color: this.colorStatus.find(
            color => color.value === appointment.appointmentStatusId
          ),
          actions: this.actions,
          meta: {
            id: appointment.id,
            status: this.appointmentStatus.find(
              status => status.id === appointment.appointmentStatusId
            )
          }
        };
      });
    });
  }

  view: string = "month";

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent("Edited", event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent("Deleted", event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  activeDayIsOpen: boolean = true;

  constructor(
    private modal: NgbModal,
    private store: Store<fromStore.OrdinationState>,
    private patientService: fromServices.PatientService,
    private clinicService: fromServices.ClinicService
  ) {}

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent("Dropped or resized", event);
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: "lg" });
  }

  onMissedClick(event) {
    let command: MarkAppointmentMissedCommand = {
      appointmentId: event.meta.id
    };
    this.store.dispatch(new fromStore.MarkAppointmentMissed(command));
  }

  onCompletedClick(event) {
    let command: MarkAppointmentCompletedCommand = {
      appointmentId: event.meta.id
    };
    this.store.dispatch(new fromStore.MarkAppointmentCompleted(command));
  }

  onDateChange(view): void {
    if (view) this.view = view;

    this.activeDayIsOpen = false;

    let startDate;
    let endDate;

    if (this.view === "month") {
      startDate = startOfMonth(this.viewDate);
      endDate = endOfMonth(this.viewDate);
    } else if (this.view === "week") {
      startDate = startOfWeek(this.viewDate);
      endDate = endOfWeek(this.viewDate);
    } else {
      startDate = startOfDay(this.viewDate);
      endDate = endOfDay(this.viewDate);
    }

    this.loadAppointments(startDate, endDate);
  }

  loadAppointments(startDate, endDate): void {
    let query: GetClinicAppointmentsQuery = {
      ClinicId: this.clinic.id,
      StartTime: startDate.toUTCString(),
      EndTime: endDate.toUTCString()
    };

    this.store.dispatch(new fromStore.LoadAppointments(query));
  }
}
