import { Component, OnInit } from "@angular/core";

import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import * as fromStore from "../../store";

import { Patient } from "../../models/patient.model";
import { GetPatientsQuery } from "../../models/queries/getPatientsQuery.model";
import { environment } from "../../../environments/environment";

import { MatDialog, MatDialogRef } from "@angular/material";
import * as fromComponents from "../../components";

@Component({
  selector: "patients",
  templateUrl: "./patients.component.html"
})
export class PatientsComponent implements OnInit {
  patients$: Observable<Patient[]>;
  pageNumber: number;
  count$: Observable<number>;
  clinicId: string;

  constructor(
    private store: Store<fromStore.OrdinationState>,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.patients$ = this.store.select(fromStore.getAllPatients);
    this.store.select(fromStore.getPatientsPageNumber).subscribe(pageNumber => {
      this.pageNumber = pageNumber;
    });
    this.count$ = this.store.select(fromStore.getPatientsCount);

    this.store.select(fromStore.getClinicId).subscribe(clinicId => this.clinicId = clinicId);

  }

  pageChange(event) {
    let query: GetPatientsQuery = {
      clinicId: this.clinicId,
      PageNumber: event,
      PageSize: 10
    };

    this.store.dispatch(new fromStore.LoadPatients(query));
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(fromComponents.PatientFormComponent, {width: '50%'})
  }
}
