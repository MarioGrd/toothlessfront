import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Clinic } from '../../models/clinic.model';
import * as fromStore from '../../store';

@Component({
  selector: 'clinic',
  templateUrl: './clinic.component.html'
})
export class ClinicComponent implements OnInit {

    clinic: Clinic;
  
    constructor(private store: Store<fromStore.OrdinationState>) { }
  
    ngOnInit() {

      this.store.select(fromStore.getClinic).subscribe((clinic) =>{
        this.clinic = clinic;
      });
      
    }
}
