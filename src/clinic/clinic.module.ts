import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {NgxChartsModule} from '@swimlane/ngx-charts';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MaterialModule } from '../app/material';

import { CalendarModule } from 'angular-calendar';

import { reducers, effects } from './store';

// guards
import * as fromGuards from './guards';

// containers
import * as fromContainers from './containers';

// components
import * as fromComponents from './components';

// services
import * as fromServices from './services';

// routes
export const ROUTES: Routes = [
    {
        path: '',
        component: fromContainers.ClinicComponent,
        canActivate: [fromGuards.ClinicGuard],
        children: 
        [
            {
                path: 'details',
                component: fromContainers.ClinicDetailsComponent,
            },
            {
            path: 'patients',
            component: fromContainers.PatientsComponent,
            canActivate: [fromGuards.PatientsGuard]
          },
          {
            path: 'procedures',
            component: fromContainers.ProceduresComponent,
            canActivate: [fromGuards.ClinicGuard]
          },
          {
            path: 'appointments',
            component: fromContainers.AppointmentsComponent,
            canActivate: [fromGuards.AppointmentGuard],
          },{
              path: 'reports',
              component: fromContainers.ReportsComponent,
              canActivate: [fromGuards.ReportGuard]
          }

        ]
      }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    NgbModule,
    CalendarModule,
    NgxChartsModule,
    RouterModule.forChild(ROUTES),
    StoreModule.forFeature('ordination', reducers),
    EffectsModule.forFeature(effects)
  ],
  providers: [...fromServices.services, ...fromGuards.guards],
  declarations: [...fromContainers.containers, ...fromComponents.components ],
  exports: [...fromContainers.containers, ...fromComponents.components],
  entryComponents: [...fromComponents.components]
})
export class ClinicModule {}