import { Component } from "@angular/core";
import {
  FormControl,
  Validators,
  FormBuilder,
  FormGroup
} from "@angular/forms";

import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import * as fromStore from "../../store";

import { AddPatientCommand } from "../../models/commands/addPatientCommand.model";
import { Clinic } from "../../models/clinic.model";

import { MatDialog, MatDialogRef } from "@angular/material";

@Component({
  selector: "patient-form",
  templateUrl: "./patient-form.component.html"
})
export class PatientFormComponent {
  clinic: Clinic;
  command: AddPatientCommand;

  firstName: FormControl;
  lastName: FormControl;
  telephoneNumber: FormControl;
  address: FormControl;
  birthDate: FormControl;
  commandForm: FormGroup;

  constructor(
    private store: Store<fromStore.OrdinationState>,
    public dialogRef: MatDialogRef<PatientFormComponent>
  ) {
    this.store
      .select(fromStore.getClinic)
      .subscribe(clinic => (this.clinic = clinic));

    this.command = {
      ClinicId: this.clinic.id,
      FirstName: "",
      LastName: "",
      Address: "",
      TelephoneNumber: "",
      BirthDate: null
    };

    this.firstName = new FormControl(this.command.FirstName, [
      Validators.required,
      Validators.maxLength(20),
      Validators.minLength(2)
    ]);
    this.lastName = new FormControl(this.command.LastName, [
      Validators.required,
      Validators.maxLength(20),
      Validators.minLength(2)
    ]);
    this.telephoneNumber = new FormControl(this.command.TelephoneNumber, [
      Validators.required
    ]);
    this.address = new FormControl(this.command.Address, []);
    this.birthDate = new FormControl(this.command.BirthDate, [
      Validators.required
    ]);

    this.commandForm = new FormGroup({
      firstName: this.firstName,
      lastName: this.lastName,
      address: this.address,
      telephoneNumber: this.telephoneNumber,
      birthDate: this.birthDate
    });
  }

  getFirstNameError() {
    if (this.firstName.hasError("required")) {
      return "First name is required.";
    }

    if (this.firstName.hasError("maxlength")) {
      return "First name too long.";
    }

    if (this.firstName.hasError("minlength")) {
      return "First name too short.";
    }
  }

  getLastNameError() {
    if (this.lastName.hasError("required")) {
      return "Last name is required.";
    }

    if (this.lastName.hasError("maxlength")) {
      return "Last name too long.";
    }

    if (this.lastName.hasError("minlength")) {
      return "last name too short.";
    }
  }

  getPhoneError() {
    if (this.telephoneNumber.hasError("required")) {
      return "Phone number is required.";
    }
  }

  getBirthDateError() {
    if (this.birthDate.hasError("required")) {
      return "Birth date is required.";
    }
  }

  submit() {
    if (this.commandForm.invalid) return;

    this.command = {
      ClinicId: this.clinic.id,
      FirstName: this.firstName.value,
      LastName: this.lastName.value,
      Address: this.address.value,
      TelephoneNumber: this.telephoneNumber.value,
      BirthDate: this.birthDate.value
    };

    this.store.dispatch(new fromStore.AddPatient(this.command));

    this.dialogRef.close();
  }
  onCloseModal(): void {
    this.dialogRef.close();
  }
}
