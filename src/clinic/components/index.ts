import { ProcedureFormComponent } from './procedures/procedure-form.component';
import { PatientFormComponent } from './patients/patient-form.component';


export const components: any[] = [ProcedureFormComponent, PatientFormComponent];

export * from './procedures/procedure-form.component';
export * from './patients/patient-form.component';