import { Component, OnInit } from "@angular/core";
import {
  FormControl,
  Validators,
  FormBuilder,
  FormGroup
} from "@angular/forms";

import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import * as fromStore from "../../store";
import * as fromServices from "../../services";

import { AddClinicProcedureCommand } from "../../models/commands/addClinicProcedureCommand.model";
import { Clinic } from "../../models/clinic.model";
import { ProcedureType } from "../../models/enums/procedure-type.model";

import { MatDialog, MatDialogRef } from "@angular/material";

@Component({
  selector: "procedure-form",
  templateUrl: "./procedure-form.component.html"
})
export class ProcedureFormComponent implements OnInit {
  ngOnInit() {
  }

  clinic: Clinic;
  command: AddClinicProcedureCommand;

  name: FormControl;
  price: FormControl;
  typeId: FormControl;
  commandForm: FormGroup;
  types: ProcedureType[];

  constructor(
    private store: Store<fromStore.OrdinationState>,
    private clinicService: fromServices.ClinicService,
    public dialogRef: MatDialogRef<ProcedureFormComponent>
  ) {
    this.types = this.clinicService.types;
    this.store.select(fromStore.getClinic).subscribe(clinic => this.clinic = clinic);
    
    this.command = {
        ClinicId: this.clinic.id,
        Name: "",
        Price: 1,
        TypeId: this.types[0].id
      };

    this.name = new FormControl(this.command.Name, [
      Validators.required,
      Validators.maxLength(20),
      Validators.minLength(2)
    ]);
    this.price = new FormControl(this.command.Price, [Validators.required]);
    this.typeId = new FormControl(this.command.TypeId, [Validators.required]);

    this.commandForm = new FormGroup({
      firstName: this.name,
      lastName: this.price,
      typeId: this.typeId
    });
  }

  getNameError() {
    if (this.name.hasError("required")) {
      return "Name is required.";
    }

    if (this.name.hasError("maxlength")) {
      return "Name too long.";
    }

    if (this.name.hasError("minlength")) {
      return "Name too short.";
    }
  }

  getPriceError() {
    if (this.price.hasError("required")) {
      return "Price is required.";
    }
  }

  getTypeError() {
    if (this.typeId.hasError("required")) {
      return "Type is required.";
    }
  }

  submit() {
    if (this.commandForm.invalid) return;

    this.command = {
      ClinicId: this.clinic.id,
      Name: this.name.value,
      Price: this.price.value,
      TypeId: this.typeId.value
    };
    this.store.dispatch(new fromStore.AddClinicProcedure(this.command));
    this.dialogRef.close();
  }

  onCloseModal(): void {
    this.dialogRef.close();
  }
}
