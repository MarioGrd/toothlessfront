import { ClinicService  } from './clinic.service';
import { PatientService } from './patient.service';
import { AppointmentService } from './appointment.service';
import { ReportService } from './report.service';

export const services: any[] = [ClinicService, PatientService, AppointmentService, ReportService];

export * from './clinic.service';
export * from './patient.service';
export * from './appointment.service';
export * from './report.service';