import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";

import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";

import { Clinic } from "../models/clinic.model";
import { Report } from "../models/report.model";

import { GetReportQuery } from "../models/queries/GetReportQuery.model";

import { environment } from "../../environments/environment";

@Injectable()
export class ReportService {
  private url: string;

  constructor(private http: HttpClient) {
    this.url = `${environment.baseUrl}/report`;
  }
  getReports(payload: GetReportQuery): Observable<Report> {
      
    const options = {
        params: new HttpParams()
          .set("ClinicId", payload.ClinicId)
          .set("ProcedureId", `${payload.ProcedureId}`)
          .set("StartTime", `${payload.StartTime}`)
          .set("EndTime", `${payload.EndTime}`)
      };

      return this.http
      .get<any>(this.url, options)
      .pipe(catchError((error: any) => Observable.throw(error.json())));
  }
}
