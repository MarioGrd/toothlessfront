import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";

import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";

import { Clinic } from "../models/clinic.model";
import { GetClinicAppointmentsQuery } from "../models/queries/GetClinicAppointmentsQuery.model";

import { MarkAppointmentMissedCommand } from '../models/commands/MarkAppointmentMissedCommand.model';
import { MarkAppointmentCompletedCommand } from '../models/commands/MarkAppointmentCompletedCommand.model';
import { AddAppointmentCommand } from '../models/commands/AddAppointmentCommand.model';
import { environment } from "../../environments/environment";

@Injectable()
export class AppointmentService {
  private url: string;

  constructor(private http: HttpClient) {
    this.url = `${environment.baseUrl}appointment/`;
  }

  getAppointments(payload: GetClinicAppointmentsQuery): Observable<any> {
    const options = {
      params: new HttpParams()
        .set("ClinicId", payload.ClinicId)
        .set("StartTime", `${payload.StartTime}`)
        .set("EndTime", `${payload.EndTime}`)
    };

    return this.http
      .get<any>(this.url, options)
      .pipe(catchError((error: any) => Observable.throw(error.json())));
  }

  markAppointmentCompleted(payload: MarkAppointmentCompletedCommand): Observable<any> {
    return this.http
      .post(`${this.url}${payload.appointmentId}/completed`, null)
      .pipe(catchError((error: any) => Observable.throw(error.json())));
  }

  markAppointmentMissed(payload: MarkAppointmentMissedCommand): Observable<any> {
    return this.http
      .post(`${this.url}${payload.appointmentId}/missed`, null)
      .pipe(catchError((error: any) => Observable.throw(error.json())));
  }

  addAppointment(payload: AddAppointmentCommand): Observable<any> {
    return this.http
      .post(this.url, payload)
      .pipe(catchError((error: any) => Observable.throw(error.json())));
  }
}
