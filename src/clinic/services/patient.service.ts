import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { RequestOptions } from "@angular/http";

import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";

import { Patient } from "../models/patient.model";
import { Paged } from "../models/paged.model";

import { GetPatientsQuery } from "../models/queries/getPatientsQuery.model";
import { SearchClinicPatientQuery } from "../models/queries/SearchClinicPatientQuery.model";
import { AddPatientCommand } from "../models/commands/addPatientCommand.model";
import { environment } from "../../environments/environment";

@Injectable()
export class PatientService {
  private url: string;

  constructor(private http: HttpClient) {
    this.url = `${environment.baseUrl}patient/`;
  }

  searchPatients(payload: SearchClinicPatientQuery) : Observable<Patient[]> {
    const options = {
        params: new HttpParams()
          .set("ClinicId", payload.clinicId)
          .set("LastName", `${payload.lastName}`)
      };

      return this.http
      .get<Patient[]>(`${this.url}search`, options)
      .pipe(
          map((response: Observable<Patient[]>) => response),
          catchError((error: any) => Observable.throw(error.json())));
  }

  getPatients(payload: GetPatientsQuery): Observable<Paged<Patient>> {
    const options = {
      params: new HttpParams()
        .set("ClinicId", payload.clinicId)
        .set("PageNumber", `${payload.PageNumber}`)
        .set("PageSize", `${payload.PageSize}`)
    };

    return this.http
      .get<Paged<Patient>>(this.url, options)
      .pipe(catchError((error: any) => Observable.throw(error.json())));
  }

  addPatient(payload: AddPatientCommand): Observable<{}> {
    return this.http
      .post<{}>(this.url, payload)
      .pipe(catchError((error: any) =>  {
        console.log(error);
        return Observable.throw(error.json());
      }));
  }

  deletePatient(payload: string): Observable<any> {
    const options = { params: new HttpParams().set("PatientId", payload) };

    return this.http.delete(this.url, options).pipe(
      catchError((error: any) => {
        console.log(error);
        return Observable.throw(error.json());
      })
    );
  }
}
