import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";

import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";

import { Clinic } from "../models/clinic.model";
import { Procedure } from "../models/procedure.model";
import { AddClinicProcedureCommand } from "../models/commands/addClinicProcedureCommand.model";
import { ProcedureType } from "../models/enums/procedure-type.model"; 
import { environment } from "../../environments/environment";

import { SearchClinicProcedureQuery } from "../models/queries/SearchClinicProcedureQuery.model";

@Injectable()
export class ClinicService {
  private url: string;

  types : ProcedureType[];

  getTypeName(typeId: number) {
       var obj: ProcedureType = this.types.find((type) => type.id === typeId);
       return obj.name;
  }
  

  constructor(private http: HttpClient) {
    this.url = `${environment.baseUrl}clinic/`;
    this.types =  [{id: 1, name: 'Half hour'}, {id: 2, name: 'Hour'}, {id: 3, name: "2 hours"}];
  }

  searchProcedures(
    payload: SearchClinicProcedureQuery
  ): Observable<Procedure[]> {
    const options = {
      params: new HttpParams()
        .set("ClinicId", payload.clinicId)
        .set("Name", `${payload.name}`)
    };

    return this.http.get<Procedure[]>(`${this.url}procedure`, options).pipe(
      map((response: Observable<Procedure[]>) => response),
      catchError((error: any) => Observable.throw(error.json()))
    );
  }

  getClinic(payload: string): Observable<Clinic> {
    return this.http
      .get<Clinic>(`${this.url}/` + payload)
      .pipe(catchError((error: any) => Observable.throw(error.json())));
  }

  addProcedure(payload: AddClinicProcedureCommand): Observable<any> {
    return this.http
      .post<any>(`${this.url}procedure`, payload)
      .pipe(catchError((error: any) => Observable.throw(error.json())));
  }
}
